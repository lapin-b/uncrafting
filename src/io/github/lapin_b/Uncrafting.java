package io.github.lapin_b;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.plugin.java.JavaPlugin;

public class Uncrafting extends JavaPlugin{
	FileConfiguration config = getConfig();
	
	public void onEnable(){
		// All config defaults
		registerConfig();
		
		registerTools();
		registerArmor();
	}

	private void addCrafting(Material input, ItemStack output){
		ShapelessRecipe recipe = new ShapelessRecipe(output);
		recipe.addIngredient(input);
		this.getServer().addRecipe(recipe);
	}
	
	private void registerTools(){
		if(config.getBoolean("tools.wood")){
			addCrafting(Material.WOOD_HOE, new ItemStack(Material.WOOD, 2));
			addCrafting(Material.WOOD_AXE, new ItemStack(Material.WOOD, 3));
			addCrafting(Material.WOOD_SPADE, new ItemStack(Material.WOOD, 1));
			addCrafting(Material.WOOD_PICKAXE, new ItemStack(Material.WOOD, 3));
			addCrafting(Material.WOOD_SWORD, new ItemStack(Material.WOOD, 2));
		}
		
		if(config.getBoolean("tools.stone")){
			addCrafting(Material.STONE_HOE, new ItemStack(Material.COBBLESTONE, 2));
			addCrafting(Material.STONE_AXE, new ItemStack(Material.COBBLESTONE, 3));
			addCrafting(Material.STONE_SPADE, new ItemStack(Material.COBBLESTONE, 1));
			addCrafting(Material.STONE_PICKAXE, new ItemStack(Material.COBBLESTONE, 3));
			addCrafting(Material.STONE_SWORD, new ItemStack(Material.COBBLESTONE, 2));
		}
		
		if(config.getBoolean("tools.iron")){
			addCrafting(Material.IRON_HOE, new ItemStack(Material.IRON_INGOT, 2));
			addCrafting(Material.IRON_AXE, new ItemStack(Material.IRON_INGOT, 3));
			addCrafting(Material.IRON_SPADE, new ItemStack(Material.IRON_INGOT, 1));
			addCrafting(Material.IRON_PICKAXE, new ItemStack(Material.IRON_INGOT, 3));
			addCrafting(Material.IRON_SWORD, new ItemStack(Material.IRON_INGOT, 2));
		}
		
		if(config.getBoolean("tools.diamond")){
			addCrafting(Material.DIAMOND_HOE, new ItemStack(Material.DIAMOND, 2));
			addCrafting(Material.DIAMOND_AXE, new ItemStack(Material.DIAMOND, 3));
			addCrafting(Material.DIAMOND_SPADE, new ItemStack(Material.DIAMOND, 1));
			addCrafting(Material.DIAMOND_PICKAXE, new ItemStack(Material.DIAMOND, 3));
			addCrafting(Material.DIAMOND_SWORD, new ItemStack(Material.DIAMOND, 2));
		}
		
		if(config.getBoolean("tools.gold")){
			addCrafting(Material.GOLD_HOE, new ItemStack(Material.GOLD_INGOT, 2));
			addCrafting(Material.GOLD_AXE, new ItemStack(Material.GOLD_INGOT, 3));
			addCrafting(Material.GOLD_SPADE, new ItemStack(Material.GOLD_INGOT, 1));
			addCrafting(Material.GOLD_PICKAXE, new ItemStack(Material.GOLD_INGOT, 3));
			addCrafting(Material.GOLD_SWORD, new ItemStack(Material.GOLD_INGOT, 2));
		}
	}
	
	private void registerArmor() {
		if(config.getBoolean("armor.leather")){
			addCrafting(Material.LEATHER_HELMET, new ItemStack(Material.LEATHER, 5));
			addCrafting(Material.LEATHER_CHESTPLATE, new ItemStack(Material.LEATHER, 8));
			addCrafting(Material.LEATHER_LEGGINGS, new ItemStack(Material.LEATHER, 7));
			addCrafting(Material.LEATHER_BOOTS, new ItemStack(Material.LEATHER, 4));
		}
		
		if(config.getBoolean("armor.iron")){
			addCrafting(Material.IRON_HELMET, new ItemStack(Material.IRON_INGOT, 5));
			addCrafting(Material.IRON_CHESTPLATE, new ItemStack(Material.IRON_INGOT, 8));
			addCrafting(Material.IRON_LEGGINGS, new ItemStack(Material.IRON_INGOT, 7));
			addCrafting(Material.IRON_BOOTS, new ItemStack(Material.IRON_INGOT, 4));
		}
		
		if(config.getBoolean("armor.diamond")){
			addCrafting(Material.DIAMOND_HELMET, new ItemStack(Material.DIAMOND, 5));
			addCrafting(Material.DIAMOND_CHESTPLATE, new ItemStack(Material.DIAMOND, 8));
			addCrafting(Material.DIAMOND_LEGGINGS, new ItemStack(Material.DIAMOND, 7));
			addCrafting(Material.DIAMOND_BOOTS, new ItemStack(Material.DIAMOND, 4));
		}
		
		if(config.getBoolean("armor.gold")){
			addCrafting(Material.GOLD_HELMET, new ItemStack(Material.GOLD_INGOT, 5));
			addCrafting(Material.GOLD_CHESTPLATE, new ItemStack(Material.GOLD_INGOT, 8));
			addCrafting(Material.GOLD_LEGGINGS, new ItemStack(Material.GOLD_INGOT, 7));
			addCrafting(Material.GOLD_BOOTS, new ItemStack(Material.GOLD_INGOT, 4));
		}
	}
	
	private void registerConfig(){
		config.addDefault("tools.wood", true);
		config.addDefault("tools.iron", true);
		config.addDefault("tools.stone", true);
		config.addDefault("tools.diamond", true);
		config.addDefault("tools.gold", true);
		
		config.addDefault("armor.leather", true);
		config.addDefault("armor.iron", true);
		config.addDefault("armor.diamond", true);
		config.addDefault("armor.gold", true);
		
		config.options().copyDefaults(true);
		config.options().header("Fichier de configuration pour le plugin Uncrafting. Les éléments décraftables sont regroupés par famille.");
		saveConfig();
	}
}
