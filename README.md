# Uncrafting
Petit plugin qui permet de décrafter des items en récupérant l'ingrédient principal.

## Décrafts possibles
- Outils
	- Bois
	- Pierre
	- Fer
	- Or
	- Diamant

- Armure
	- Cuir
	- Fer
	- Or
	- Diamant

